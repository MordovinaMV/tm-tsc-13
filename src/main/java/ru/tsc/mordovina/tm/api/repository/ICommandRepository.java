package ru.tsc.mordovina.tm.api.repository;

import ru.tsc.mordovina.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
